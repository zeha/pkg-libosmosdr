Source: libosmosdr
Section: libs
Priority: extra
Maintainer: A. Maitland Bottoms <bottoms@debian.org>
Build-Depends: cmake,
               debhelper (>= 9.0.0~),
               libusb-1.0-0-dev [linux-any],
               libusb-dev [hurd-i386],
               libusb2-dev [kfreebsd-any],
               pkg-config
Standards-Version: 3.9.6
Homepage: http://sdr.osmocom.org/trac/
Vcs-Git: git://anonscm.debian.org/users/bottoms/pkg-libosmosdr.git
Vcs-Browser: http://anonscm.debian.org/cgit/users/bottoms/pkg-libosmosdr.git/

Package: libosmosdr-dev
Section: libdevel
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: libosmosdr0 (= ${binary:Version}), ${misc:Depends}
Description: Software defined radio support for OsmoSDR hardware (development files)
 OsmoSDR is a 100% Free Software based small form-factor inexpensive
 SDR (Software Defined Radio) project.
 .
 The hardware part of OsmoSDR brings information from an antenna connector
 to a USB plug.
 .
 This package is the software that provides control of the USB hardware
 and an API to pass data to software defined radio applications on the host.
 .
 This package contains development files.

Package: libosmosdr0
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: Software defined radio support for OsmoSDR hardware (library)
 OsmoSDR is a 100% Free Software based small form-factor inexpensive
 SDR (Software Defined Radio) project.
 .
 The hardware part of OsmoSDR brings information from an antenna connector
 to a USB plug.
 .
 This package is the software that provides control of the USB hardware
 and an API to pass data to software defined radio applications on the host.
 .
 This package contains the shared library.

Package: osmo-sdr
Architecture: any
Depends: libosmosdr0 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: Software defined radio support for OsmoSDR hardware (tools)
 OsmoSDR is a 100% Free Software based small form-factor inexpensive
 SDR (Software Defined Radio) project.
 .
 The hardware part of OsmoSDR brings information from an antenna connector
 to a USB plug.
 .
 This package is the software that provides control of the USB hardware
 and an API to pass data to software defined radio applications on the host.
 .
 This package contains a set of command line utilities:
